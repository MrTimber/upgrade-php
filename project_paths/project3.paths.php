<?php

use Rector\Config\RectorConfig;

/**
 * @var string $webRootPath
 * @var RectorConfig $rectorConfig
 */

$projectPath = $webRootPath . '/project3';

// Static Reflection And Autoload - see https://getrector.com/documentation/static-reflection-and-autoload
$rectorConfig->autoloadPaths([
    //$projectPath . '/classes',
]);

// Define Paths - see https://getrector.com/documentation/define-paths
$rectorConfig->paths([
    $projectPath,
]);

// Ignoring Paths - See https://getrector.com/documentation/ignoring-rules-or-paths
$rectorConfig->skip([
//    $projectPath . '/cache',
//    $projectPath . '/images',
//    $projectPath . '/lib',
//    $projectPath . '/style',
]);
