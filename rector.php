<?php

include "vendor/autoload.php";

use Rector\Config\RectorConfig;
use Rector\ValueObject\PhpVersion;
use Rector\Set\ValueObject\LevelSetList;

return static function (RectorConfig $rectorConfig): void {
    $webRootPath = realpath(__DIR__ . '/..');

    // Php Version Features - see https://getrector.com/documentation/php-version-features
    $rectorConfig->phpVersion(PhpVersion::PHP_83);

    // Import names - see https://getrector.com/documentation/import-names
    $rectorConfig->importNames();
    $rectorConfig->importShortClasses(false);

    // Set list of rules - see https://getrector.com/documentation/set-lists
    // tip: use "SetList" class to autocomplete sets with your IDE
    // Rules Overview: https://github.com/rectorphp/rector/blob/main/docs/rector_rules_overview.md
    $rectorConfig->sets([
        LevelSetList::UP_TO_PHP_83,
    ]);

    // Ignore rules - See https://getrector.com/documentation/ignoring-rules-or-paths
    $rectorConfig->skip([
    ]);

    /** Project paths - YOU MUST INCLUDE ONLY ONE PROJECT AT A TIME **/
    include 'project_paths/project1.paths.php';
//    include 'project_paths/project2.paths.php';
//    include 'project_paths/project3.paths.php';
};
