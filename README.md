# Upgrade PHP

This project is an helper to upgrade the PHP version of a project

It uses [Rector](https://github.com/rectorphp/rector)

## PHP version

Rector needs PHP >= 7.2 and can upgrade a project from from PHP >=5.3 to PHp <=8.2

## Installation

```bash
composer install
```

### Plugins

Rector may use some plugins to upgrade specific libraries.

By example to upgrade from PHP Office to PhpSpreadsheet, you can install the following packages :
```bash
composer require rector/rector-phpoffice phpoffice/phpspreadsheet --dev
```
and then use the newly added rules.


## Configuration
Adapt [rector.php](rector.php) following the [Rector documentation](https://getrector.com/documentation/)

## Draft run
```bash
php vendor/bin/rector process --dry-run
```

## Run
```bash
php vendor/bin/rector process
```